Source: libqcpp
Maintainer: Debian Med Packaging <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Kevin Murray <spam@kdmurray.id.au>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake,
               zlib1g-dev,
               libbz2-dev,
               libyaml-cpp-dev,
               libboost-dev
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/med-team/libqcpp
Vcs-Git: https://salsa.debian.org/med-team/libqcpp.git
Homepage: https://github.com/kdmurray91/libqcpp

Package: libqcpp-dev
Architecture: any
Section: libdevel
Depends: libqcpp0 (= ${binary:Version}),
         ${misc:Depends}
Description: C++11 library for NGS data quality control -- headers
 A C++ library for next-gen sequencing data quality control.
 .
 QC++ implements adaptor trimming, windowed quality trimming, length filtering
 and trimming, format conversion, and basic read manipulation. It also provides
 classes for impelementation of new measures and easy creation of bespoke
 filtering tools.
 .
 This package provides the development headers and static archive.

Package: libqcpp0
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: C++11 library for NGS data quality control
 A C++ library for next-gen sequencing data quality control.
 .
 QC++ implements adaptor trimming, windowed quality trimming, length filtering
 and trimming, format conversion, and basic read manipulation. It also provides
 classes for impelementation of new measures and easy creation of bespoke
 filtering tools. All classes are able to report their results in a machine and
 human readable format for easy aggregation.
 .
 This package provides the shared library.

Package: gbsqc
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqcpp0 (= ${binary:Version})
Description: GBS sequencing data quality control tool
 GBSQC is a tool that performs NGS quality control targeted at genotyping by
 sequencing data. It performs read merging, read adaptor trimming, windowed
 quality score trimming and optional length filtering, and can provide YAML
 reports of its actions.
 .
 GBSQC is implemented using the libqc++ (or libqcpp) library.
